package com.pragma.monolitico.posada.app.data;

import com.pragma.monolitico.posada.app.entity.City;

public class CityData {


    public static  final City cityArmenia = City.builder().id(1l).name("armenia").build();
    public static  final City cityPereira = City.builder().id(2l).name("pereira").build();
    public static  final City cityManizales = City.builder().id(3l).name("manizales").build();
    public static  final City cityBogota = City.builder().id(4l).name("bogota").build();



}
