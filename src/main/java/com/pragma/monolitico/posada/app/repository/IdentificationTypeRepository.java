package com.pragma.monolitico.posada.app.repository;

import com.pragma.monolitico.posada.app.entity.IdentificationType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IdentificationTypeRepository extends JpaRepository<IdentificationType,Long> {
}
