package com.pragma.monolitico.posada.app.exceptions;

public class ApiRequestException extends RuntimeException{

    public ApiRequestException(String message) {

        super(message);
    }
}