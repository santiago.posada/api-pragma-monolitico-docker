package com.pragma.monolitico.posada.app.exceptions.client;

public class ClientIdNumberException extends  RuntimeException{

    public ClientIdNumberException(String identificationType){
        super(String.format("The id : %s has to be numbers only",identificationType));
    }
}
