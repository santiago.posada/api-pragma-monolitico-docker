package com.pragma.monolitico.posada.app.exceptions.global;

public class GlobalDataRequiredException extends  RuntimeException{

    public GlobalDataRequiredException (){
        super(String.format("enter all the required data"));
    }




}
